from django.contrib import admin

# Importing models to admin dashboard
from .models import Choice, Question


# Adding choices inside question section CLASS which will be called in QuestionAdmin CLASS
class ChoiceInline(admin.StackedInline):
    model = Choice
    extra = 3

# Separated fieldsets with Choiceses!
class QuestionAdmin(admin.ModelAdmin):
    list_filter = ['pub_date']
    search_fields = ['question_text']
    fieldsets = [
        ('Question',               {'fields': ['question_text']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [ChoiceInline]

# Or you can just put it without field sets and choices like below
    # class QuestionAdmin(admin.ModelAdmin):
    #    fields = ['pub_date', 'question_text']

admin.site.register(Question, QuestionAdmin)

# Separated Choices 
    # admin.site.register(Choice)